**IMPORTANT (Apr 2024):Go to the subdirectory `sprawl-tileserver-new-docker` for the updated dockerfile and the updated instructions**
See https://github.com/maptiler/tileserver-gl for the (updated) parent repo from which this was adapted.

# TileServer GL


Vector and raster maps with GL styles. Server side rendering by Mapbox GL Native. Map tile server for Mapbox GL JS, Android, iOS, Leaflet, OpenLayers, GIS via WMTS, etc.

## Get Started

Make sure you have Node installed

Clone the repository

```
https://gitlab.com/cpbl/sprawl-tileserver.git
```

Add your current user to docker group (or ask your admin to do this)

```
sudo usermod -aG docker $USER
```

Switch session to docker group

```
newgrp - docker
```

Build Docker image (I am using sumbaldocker as name of the image. It can be changed)
```
docker build -f Dockerfile . -t sumbaldocker
```

To run this docker Image, you will need two file
* mbtiles 
* config file.

By default when running this docker image, it generates a config file and automatically downloads the data for a certain region and start a ready to use container on your computer and the maps are going to be available in webbrowser on localhost:8080.

An example of a config file can found in config-file directory.

!!: What is "config-file directory?"

In this config file, you can define the styles and name of the mbtiles.

One you have the mbtiles and config file, run the following command to run docker image

```
docker run --rm -it -v $(pwd):/data -p 8080:80 sumbaldocker final.mbtiles -c conf.json -V
```

where 

sumbaldocker: name of docker image created

final.mbtiles: name of mbtiles.

config.json: name of json file (located in config-file directory). !!: Again, what is this directory?


cpbl tried:
```
# Following fails; it must be a hard link / local file? Or what if we specify full path in the conf.json
# ln -s ../generate-tiles/tiledata/final.mbtiles final.mbtiles

sudo docker run --rm -it -v $(pwd):/data -p 8080:80 cpbldocker final.mbtiles -c conf.json -V
```
seems to run on 8080 (not yet visible on Internet)


### Important Notes:
This repo contains a special npm package called `sumbal-tileserver-gl-styles`. This helps docker to install/download fonts and map styles. [More info](https://gitlab.com/cpbl/sprawl-tileserver).

## Documentation

You can read full documentation of this project at http://tileserver.readthedocs.io/.
